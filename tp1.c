#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <pthread.h>
#include <time.h>
#include <errno.h>

#include <glibtop.h>
#include <glibtop/cpu.h>
#include <glibtop/mem.h>
#include <glibtop/proclist.h>

#define N 5
#define PENSAR 0
#define FOME 1
#define COMER 2
#define ESQUERDA (nfilosofo+4)%N //agarrar garfo
 //da esquerda
#define DIREITA (nfilosofo+1)%N //agarrar garfo
 //da direita
void *filosofo(void *num);
void agarraGarfo(int);
void deixaGarfo(int);
void testar(int);

sem_t mutex;
sem_t S[N]; //inicializacao do semáforo
int estado[N];
int nfilosofo[N]={0,1,2,3,4};


void my_sleep(unsigned msec) {
    struct timespec req, rem;
    int err;
    req.tv_sec = msec / 1000;
    req.tv_nsec = (msec % 1000) * 1000000;
    while ((req.tv_sec != 0) || (req.tv_nsec != 0)) {
        if (nanosleep(&req, &rem) == 0)
            break;
        err = errno;
        // Interrupted; continue
        if (err == EINTR) {
            req.tv_sec = rem.tv_sec;
            req.tv_nsec = rem.tv_nsec;
        }
        // Unhandleable error (EFAULT (bad pointer), EINVAL (bad timeval in tv_nsec), or ENOSYS (function not supported))
        break;
    }
}


void *filosofo(void *num)
{
 while(1)
{
 int *i = (int*) num;
 my_sleep(1);
 agarraGarfo(*i);
 my_sleep(1);
 deixaGarfo(*i);
 }
}
void agarraGarfo(int nfilosofo)
{
 sem_wait(&mutex);
 estado[nfilosofo] = FOME;
 printf("Filosofo %d tem fome.\n",nfilosofo+1);
 //+1 para imprimir filosofo 1 e nao filosofo 0
 testar(nfilosofo);
 sem_post(&mutex);
 sem_wait(&S[nfilosofo]);
 my_sleep(1);
}

void deixaGarfo(int nfilosofo)
{
 sem_wait(&mutex);
 estado[nfilosofo]=PENSAR;
 printf("Filosofo %d deixou os garfos %d e % d.\n",nfilosofo+1,ESQUERDA+1,nfilosofo+1);
 printf("Filosofo %d esta a pensar.\n",nfilosofo+1);
 testar(ESQUERDA);
 testar(DIREITA);
 sem_post(&mutex);
}
void testar(int nfilosofo)
{
 if(estado[nfilosofo]==FOME && estado[ESQUERDA]  !=COMER && estado[DIREITA]!=COMER)
{
 estado[nfilosofo]=COMER;
 my_sleep(2);
 printf("Filosofo %d agarrou os garfos %d e  %d.\n",nfilosofo+1,ESQUERDA+1,nfilosofo+1);
 printf("Filosofo %d esta a comer.\n",nfilosofo+1);
 sem_post(&S[nfilosofo]);
 }
}

int main()
{
 int i;
 glibtop_init();

glibtop_cpu cpu;
glibtop_mem memory;
glibtop_proclist proclist;

glibtop_get_cpu (&cpu);
glibtop_get_mem(&memory);

 pthread_t thread_id[N]; //identificadores das //threads
 		sem_init(&mutex,0,1);
 			for(i=0;i<N;i++)
 		sem_init(&S[i],0,0);
 			for(i=0;i<N;i++)
{
 pthread_create(&thread_id[i],NULL,filosofo,&nfilosofo[i]);
 //criar as threads
 printf("Filosofo %d esta a pensar.\n",i+1);
 }

 for(i=0;i<N;i++)pthread_join(thread_id[i],NULL); //para
 //fazer a junção das threads
 return(0);
}